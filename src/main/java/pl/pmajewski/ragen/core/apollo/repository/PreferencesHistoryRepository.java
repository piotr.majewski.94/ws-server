package pl.pmajewski.ragen.core.apollo.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pmajewski.ragen.core.apollo.model.PreferencesHistory;

public interface PreferencesHistoryRepository extends CrudRepository<PreferencesHistory, Long> {

}
