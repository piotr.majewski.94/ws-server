package pl.pmajewski.ragen.core.apollo.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import pl.pmajewski.ragen.core.apollo.model.Participant;
import pl.pmajewski.ragen.core.apollo.repository.ParticipantRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

public class ParticipantRepositoryCustomImpl implements ParticipantRepositoryCustom {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Optional<Participant> getFirst() {
        String hql = "from Participant";
        Query query = entityManager.createQuery(hql);
        query.setMaxResults(1);
        List<Participant> list = query.getResultList();

        if(list.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(list.get(0));
        }
    }

    @Override
    public Optional<Participant> getFirstOtherThan(Participant participant) {
        String hql = "select p from Participant p where p.id!=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", participant.getId());
        query.setMaxResults(1);
        List<Participant> list = query.getResultList();

        if (list.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(list.get(0));
        }
    }
}
