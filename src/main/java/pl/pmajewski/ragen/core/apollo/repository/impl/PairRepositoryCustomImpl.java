package pl.pmajewski.ragen.core.apollo.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import pl.pmajewski.ragen.core.apollo.model.Pair;
import pl.pmajewski.ragen.core.apollo.repository.PairRepositoryCustom;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

public class PairRepositoryCustomImpl implements PairRepositoryCustom {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Optional<Pair> getLastPairByDeviceIdentification(DeviceIdentification deviceIdentification) {
        String q = "from Pair p where (p.deviceOne = :identification or p.deviceTwo = :identification) and p.startTimestamp >= :timestamp";
        Query query = entityManager.createQuery(q);
        query.setParameter("identification", deviceIdentification);
        query.setParameter("timestamp", deviceIdentification.getConnectTimestamp());
        List<Pair> resultList = query.getResultList();

        if(!resultList.isEmpty()) {
            return Optional.of(resultList.get(0));
        }
        return Optional.empty();
    }
}
