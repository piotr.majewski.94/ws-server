package pl.pmajewski.ragen.core.apollo.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "pairs", schema = "randomizer")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Pair {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "participant_one")
    private DeviceIdentification deviceOne;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "participant_two")
    private DeviceIdentification deviceTwo;

    @Column(name = "start_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar startTimestamp;

    @Column(name = "end_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar endTimestamp;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pair", cascade = CascadeType.ALL)
    private List<PreferencesHistory> preferencesHistories;

    @PrePersist
    private void prePersist() {
        startTimestamp = Calendar.getInstance();
    }
}
