package pl.pmajewski.ragen.core.apollo.service;

import pl.pmajewski.ragen.core.apollo.pojo.ApolloMessagePojo;

public interface PairService {

    Long getActivePairDevice(Long deviceIdentification);

    void announceNewPair(Long participantOneId, Long participantTwoId);

    void disconnect(Long deviceInitializer);

    void disconnectByPairId(Long pairId);

    void sendMessage(Long deviceIdentificationId, ApolloMessagePojo message);
}
