package pl.pmajewski.ragen.core.apollo.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;
import pl.pmajewski.ragen.core.utils.Sex;

import javax.persistence.*;

@Entity
@Table(name = "preferences_history", schema = "randomizer")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class PreferencesHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "device_identification_id")
    private DeviceIdentification deviceIdentification;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "pair_id")
    private Pair pair;

    @Column
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(name =  "longitude")
    private String longitude;

    @Column(name = "latitude")
    private String latitude;

    @Column
    @Enumerated(EnumType.STRING)
    private Sex preferedSex;
}
