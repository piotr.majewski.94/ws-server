package pl.pmajewski.ragen.core.apollo.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pmajewski.ragen.core.apollo.model.Participant;
import pl.pmajewski.ragen.core.initializer.model.DeviceIdentification;

import java.util.Optional;

public interface ParticipantRepository extends CrudRepository<Participant, Long>, ParticipantRepositoryCustom {

    Optional<Participant> getByIdentification(DeviceIdentification identification);
}
