package pl.pmajewski.ragen.core.apollo.pojo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ApolloMessagePojo {

    private Long id;
    private String contentType;
    private String content;
    private LocalDateTime date;
}
