package pl.pmajewski.ragen.core.user.utils.randomizer;

import pl.pmajewski.ragen.core.user.pojo.UserSimplePojo;
import pl.pmajewski.ragen.core.user.utils.randomizer.exceptions.ConverserSelectedAlreadyException;

public interface ConverserRandomizer {

	void init(Long userId);
	
	UserSimplePojo randConverser(Long userId) throws ConverserSelectedAlreadyException;
}
