package pl.pmajewski.ragen.core.user.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.ragen.core.user.pojo.UserSimplePojo;
import pl.pmajewski.ragen.core.user.pojo.rest.LogInPojo;
import pl.pmajewski.ragen.core.user.service.UserAnonymousService;

@RestController
@RequestMapping(value = "/user")
@CommonsLog
public class UserAnonymousController {
	
	private UserAnonymousService userService;
	
	public UserAnonymousController(UserAnonymousService userService) {
		this.userService = userService;
	}
	
	@RequestMapping(value = "/anonymous/initialize", method = RequestMethod.POST)
	public UserSimplePojo initiliazeUser(@RequestBody LogInPojo body) {
//		log.debug("initilizeUser() ->  body: "+body);
		UserSimplePojo response = userService.initialize(body);
//		log.info("user: "+response+" successfully logged");
		return response;
	}
}
