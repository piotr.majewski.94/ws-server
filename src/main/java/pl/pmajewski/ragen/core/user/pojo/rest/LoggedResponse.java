package pl.pmajewski.ragen.core.user.pojo.rest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pmajewski.ragen.core.user.pojo.UserSimplePojo;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LoggedResponse extends UserSimplePojo {

	private String token;

}
