package pl.pmajewski.ragen.core.user.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserSimplePojo {
	
	private Long id;
	private String sex;
	private String interested; 
	private String status;
	private Boolean online;
	
}
