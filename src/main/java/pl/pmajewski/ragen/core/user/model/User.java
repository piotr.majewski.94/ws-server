package pl.pmajewski.ragen.core.user.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.pmajewski.ragen.core.user.pojo.UserSimplePojo;

@Entity
@Table(schema = "public", name = "users")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@ToString(exclude = {"actualLocation"})
@EqualsAndHashCode(of = {"id"})
public class User {
	
	public enum Sex {MALE, FEMALE, ANY}
	
	public enum Status {RANDOM_SEARCH, ACTIVE_CONVERSATION, SUSPEND}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column
	@Enumerated(EnumType.STRING)
	private Sex sex;
	
	@Column
	@Enumerated(EnumType.STRING)
	private Sex interested;
	
	@Column
	private Boolean online; // true when message websocket is active
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "user", cascade = {CascadeType.ALL})
	@JoinColumn(name = "actual_location")
	private UserLocation actualLocation;
	
	public void addLocation(UserLocation location) {
		actualLocation = location;
	}
	
	public UserSimplePojo getSimplePojo() {
		UserSimplePojo p = new UserSimplePojo();
		
		p.setId(this.id);
		p.setSex(this.sex.toString());
		p.setInterested(this.interested.toString());
		p.setStatus(this.status != null ? this.status.toString() : "");
		
		return p;
	}
}
