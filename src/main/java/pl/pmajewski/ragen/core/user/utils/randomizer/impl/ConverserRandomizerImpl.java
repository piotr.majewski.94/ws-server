package pl.pmajewski.ragen.core.user.utils.randomizer.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.ragen.core.user.exceptions.UserNotFoundException;
import pl.pmajewski.ragen.core.user.model.User;
import pl.pmajewski.ragen.core.user.model.User.Sex;
import pl.pmajewski.ragen.core.user.model.User.Status;
import pl.pmajewski.ragen.core.user.pojo.UserSimplePojo;
import pl.pmajewski.ragen.core.user.repository.UserRepository;
import pl.pmajewski.ragen.core.user.utils.randomizer.ConverserRandomizer;
import pl.pmajewski.ragen.core.user.utils.randomizer.exceptions.ConverserSelectedAlreadyException;

@Component
public class ConverserRandomizerImpl implements ConverserRandomizer {
	
	private UserRepository userRepository;
	
	public ConverserRandomizerImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void init(Long userId) {
		changeStatus(userId, User.Status.RANDOM_SEARCH);
	}

	public void changeStatus(Long userId, Status status) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new UserNotFoundException("User "+userId+" not found"));
		System.out.println("ConverserRandomizerImpl -> changeStatus() -> user: "+user);
		user.setStatus(status);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public synchronized UserSimplePojo randConverser(Long userId) throws ConverserSelectedAlreadyException {
		User user = userRepository.findById(userId).orElseThrow(() ->  new UserNotFoundException("User "+userId+" not found"));
		checkIfUserWasSelected(user);
		List<User> users = listUsers(user);
		
		if(!users.isEmpty()) {
			User converser = selectConverser(users);
			changeStatus(userId, Status.ACTIVE_CONVERSATION);
			changeStatus(converser.getId(), Status.ACTIVE_CONVERSATION);
			
			return converser.getSimplePojo();
		} else {
			return null;
		}
	}
	
	private void checkIfUserWasSelected(User user) throws ConverserSelectedAlreadyException {
		if(user.getStatus().equals(Status.ACTIVE_CONVERSATION)) {
			throw new ConverserSelectedAlreadyException();
		}
	}
	
	private User selectConverser(List<User> users) {
		Random rand = new Random();
		int idx = rand.nextInt(users.size());
		return users.get(idx);
	}
	
	private List<User> listUsers(User user) {
		List<User> out = new ArrayList<>();
		System.out.println("--- --- --- --- --- ---");
		
		if(user.getInterested() != Sex.ANY) {
			System.out.println("Out from GENDER");
			out = userRepository.findByStatusAndSexAndInterested(Status.RANDOM_SEARCH, user.getInterested(), user.getSex());
		} else {
			System.out.println("Out from ANY");
			out = userRepository.findByStatusAndInterested(Status.RANDOM_SEARCH, Sex.ANY);
		}
		System.out.println("User -> "+user.getId()+" out:");
		out.stream().forEach(i -> System.out.println(" |-- "+i));
		
		List<User> collect = out.parallelStream().filter(i -> i.getId() != user.getId()).collect(Collectors.toList());
		System.out.println("User -> "+user.getId()+" collect:");
		collect.stream().forEach(i -> System.out.println(" |-- "+i));
		
		return collect;
	}
}
