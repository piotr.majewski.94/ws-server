package pl.pmajewski.ragen.core.user.pojo.rest;

import lombok.Data;

@Data
public class LogInPojo {

	private String sex;
	private String interested;
	private Double longitude;
	private Double latitude;
	
}
