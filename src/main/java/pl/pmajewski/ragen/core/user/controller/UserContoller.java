package pl.pmajewski.ragen.core.user.controller;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.ragen.core.user.service.UserChatService;

@Controller
@CommonsLog
public class UserContoller {

	private UserChatService userService;
	
	public UserContoller(UserChatService userService) {
		this.userService = userService;
	}
	
	@MessageMapping("user/{id}/randConverser")
	public void randConverser(@DestinationVariable Long id) {
//		log.debug("randConverser for: "+id);
		this.userService.randConverser(id);
	}
}
