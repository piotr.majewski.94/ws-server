package pl.pmajewski.ragen.core.user.service.impl;


import javax.transaction.Transactional;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.ragen.core.config.GlobalProperties;
import pl.pmajewski.ragen.core.user.exceptions.UserNotFoundException;
import pl.pmajewski.ragen.core.user.model.User;
import pl.pmajewski.ragen.core.user.pojo.UserSimplePojo;
import pl.pmajewski.ragen.core.user.repository.UserRepository;
import pl.pmajewski.ragen.core.user.service.UserChatService;
import pl.pmajewski.ragen.core.user.utils.randomizer.ConverserRandomizer;
import pl.pmajewski.ragen.core.user.utils.randomizer.exceptions.ConverserSelectedAlreadyException;

@Service
@CommonsLog
public class UserChatServiceImpl implements UserChatService {

	private ConverserRandomizer randomizer;
	private SimpMessagingTemplate simpTemplate;
	private UserRepository userRepository;
	
	public UserChatServiceImpl(ConverserRandomizer randomizer, SimpMessagingTemplate simpTemplate, UserRepository userRepository) {
		this.randomizer = randomizer;
		this.simpTemplate = simpTemplate;
		this.userRepository = userRepository;
	}
	
	@Override
	@Transactional
	public void randConverser(Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User with id "+userId+" not found."));
		randomizer.init(userId);
		
		UserSimplePojo converser = null;
		long roundStart = System.currentTimeMillis();
		while(converser == null && !breakRoundTime(roundStart)) {
			try {
				converser = randomizer.randConverser(userId);
			} catch (ConverserSelectedAlreadyException e) {
				log.debug(e);
				break;
			}
		}
		
		processConverser(user.getSimplePojo(), converser);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void processConverser(UserSimplePojo user, UserSimplePojo converser) {
		if(converser != null) {
			converserEstablished(user, converser);
		} else {
			converserNotFound(user, converser);
		}
	}
	
	private void converserNotFound(UserSimplePojo user, UserSimplePojo converser) {
		log.debug("converserNotFound() -> userId: "+user.getId()+" | converser: "+converser);
	}
	
	private void converserEstablished(UserSimplePojo user, UserSimplePojo converser) {
		log.debug("converserEstablished() -> userId: "+user.getId()+" | converser: "+converser);
		simpTemplate.convertAndSend("/queue/user/"+user.getId()+"/randConverser", converser);
		simpTemplate.convertAndSend("/queue/user/"+converser.getId()+"/randConverser", user);
	}
	
	private boolean breakRoundTime(long begin) {
		if(System.currentTimeMillis() - begin > GlobalProperties.CHAT_ROUND_TIME) {
			return true;
		} else {
			return false;
		}
	}
}
