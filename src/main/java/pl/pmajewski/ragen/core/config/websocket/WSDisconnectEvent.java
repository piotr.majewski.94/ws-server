package pl.pmajewski.ragen.core.config.websocket;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import pl.pmajewski.ragen.core.config.websocket.model.WebsocketSessionData;
import pl.pmajewski.ragen.core.initializer.service.InitializerService;

@Component
@AllArgsConstructor
public class WSDisconnectEvent implements ApplicationListener<SessionDisconnectEvent> {

    private InitializerService initializerService;
    private WebsocketSessionData wsData;

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        initializerService.closeConnection(wsData.getInitializerId());
    }
}
