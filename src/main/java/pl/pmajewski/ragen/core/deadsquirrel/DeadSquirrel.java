package pl.pmajewski.ragen.core.deadsquirrel;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class DeadSquirrel {
	
	@MessageMapping("/master")
	private void masterMsg() {
		System.out.println("======== ======== ======== ========");
		System.out.println("DeadSquirrel -> masterMsg()");
		System.out.println("======== ======== ======== ========");
	}
}
