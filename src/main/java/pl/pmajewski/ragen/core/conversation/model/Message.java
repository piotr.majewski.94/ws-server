package pl.pmajewski.ragen.core.conversation.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import pl.pmajewski.ragen.core.user.model.User;

@Data
@Entity
@Table(schema = "public", name = "Messages")
public class Message {
	
	@Id
	@Column
	private BigInteger messageID;

	@Column
	private Date timestamp;

	@Column
	private String message;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private User authorID;

}
