package pl.pmajewski.ragen.core.conversation.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.pmajewski.ragen.core.user.model.User;

@Getter
@Setter
@ToString
public class ConversationSimplePojo {

	private User user1ID;
	private User user2ID;
	private Long messageID;
}
