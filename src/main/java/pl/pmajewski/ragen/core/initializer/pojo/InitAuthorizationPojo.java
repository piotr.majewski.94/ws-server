package pl.pmajewski.ragen.core.initializer.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class InitAuthorizationPojo {

    private String deviceId;
}
