package pl.pmajewski.ragen.core.initializer.service;

import pl.pmajewski.ragen.core.initializer.pojo.InitAuthorizationResponsePojo;

public interface InitializerService {

    InitAuthorizationResponsePojo initialize(String deviceId);

    void closeConnection(Long initializerId);
}
