package pl.pmajewski.ragen.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RagenCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(RagenCoreApplication.class, args);
	}
}
